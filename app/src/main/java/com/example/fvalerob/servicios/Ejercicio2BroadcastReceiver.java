package com.example.fvalerob.servicios;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Valero on 19/11/2017.
 */

public class Ejercicio2BroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent servicio = new Intent(context,ServicioEjercicio2.class);
        context.startService(servicio);

    }
}
