package com.example.fvalerob.servicios;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Toast;
import android.os.Handler;

/**
 * Created by Valero on 19/11/2017.
 */

public class ServicioEjercicio2 extends IntentService {
    private Handler miTarea;
    private int mContador = 1;

    public ServicioEjercicio2(){
        super("ServicioEjercicio2");
        miTarea = new Handler();//Creamos la tarea.

    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        for(; mContador <= 10; mContador++){
            miTarea.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),	"Contador "+mContador, Toast.LENGTH_SHORT).show();

                }
            });
            try{
                Thread.sleep(5000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mContador = 11;
        Toast.makeText(getApplicationContext(),	"Detenido", Toast.LENGTH_SHORT).show();
    }
}

