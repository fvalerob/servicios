package com.example.fvalerob.servicios;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Valero on 19/11/2017.
 */

public class Ejercicio2Activity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        Button botonIniciar = (Button) findViewById(R.id.botonIniciar);
        Button botonDetener = (Button) findViewById(R.id.botonDetener);

        botonIniciar.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick (View view) {
                startService(new Intent(Ejercicio2Activity.this, ServicioEjercicio2.class ));
            }
        });
        botonDetener.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(Ejercicio2Activity.this, ServicioEjercicio2.class ));
            }
        });
    }
}

