package com.example.fvalerob.servicios;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by Valero on 19/11/2017.
 */

public class ServicioEjercicio1 extends Service {
    MiTarea miTarea;
    private int mContador = 1;

    @Override
    public void onCreate(){
        super.onCreate();
        miTarea = new MiTarea();//Creamos la tarea.
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(miTarea.getStatus() != AsyncTask.Status.RUNNING)
            miTarea.execute();
        return Service.START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Sin implementar");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mContador = 11;
        Toast.makeText(getApplicationContext(),	"Detenido", Toast.LENGTH_SHORT).show();
    }
    private class MiTarea extends AsyncTask<Void, Integer, Void>
    {
        @Override
        protected Void doInBackground(Void... params) {
            for(; mContador <= 10; mContador++){
                publishProgress(mContador); // llama a onProgressUpdate
                try{
                    Thread.sleep(5000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // Actualizar las notificaciones UI
            Toast.makeText(getApplicationContext(),	"Contador "+values[0], Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Void param) {
            stopSelf();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            stopSelf();
        }
    }

}
